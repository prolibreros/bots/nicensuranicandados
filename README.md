# Bot #NiCensuraNiCandados

Este bot envía un correo con comentarios para la CNDH con solo
hacer RT. El nombre del destinario es el nombre de usuario de
TW.

## Uso

1. Coloca las llaves y tokens (`client`) en `get_users.rb`.
2. Coloca el ID del tweet (`$tweet`) al que se tiene que hacer RT en
   `get_users.rb`.
3. Configura el servidor de correos (`Mail.defaults`) en `send-emails.rb`.
3. Coloca este repo en el servidor.
4. Configura un trabajo CRON o similar para que cada cierto tiempo obtenga los
   usuarios que han hecho RT y otro cada tanto para que envíe los correos.

## Notas

### Almacenamiento de datos

Para evitar duplicados, se van almacenando listas de usuarios:

* `./src/users_new.txt` añade los usuarios que han hecho RT.
* `./src/users_old.txt` almacena los usuarios que ya mandaron correo.

Solo manda correo de los usuarios que aún no pertenecen a `users_old.txt`.
Una vez que se manda, el usuario pasa a pertenecer a esta lista
para evitar envíos duplicados.

### Aleatorización del texto

El texto del asunto (`./src/subjects.txt`) y cuerpo (`./src/body.html`)
del correo no es siempre el mismo.

Adentro existen cadena de caracteres como este ejemplo:

```
@[las personas,la gente, las mujeres] @[usuarias,internautas] de internet no somos @[solo,únicamente] @[consumidoras,clientas] @[pasivas,indiferentes]
```

La función `random_text` en `send-email.rb` elige de manera aleatoria
una opción de varias posibles acotadas con la sintaxis `@[opt1,opt2,opt3]`.

Entre más `@[n]` mayor aleatoriedad. El ejemplo anterior implica
las siguientes oraciones posibles:

```
las personas usuarias de internet no somos solo consumidoras pasivas
las personas usuarias de internet no somos solo consumidoras indiferentes
las personas usuarias de internet no somos solo clientas pasivas
las personas usuarias de internet no somos solo clientas indiferentes
las personas usuarias de internet no somos únicamente consumidoras pasivas
las personas usuarias de internet no somos únicamente consumidoras indiferentes
las personas usuarias de internet no somos únicamente clientas pasivas
las personas usuarias de internet no somos únicamente clientas indiferentes
las personas internautas de internet no somos solo consumidoras pasivas
las personas internautas de internet no somos solo consumidoras indiferentes
las personas internautas de internet no somos solo clientas pasivas
las personas internautas de internet no somos solo clientas indiferentes
las personas internautas de internet no somos únicamente consumidoras pasivas
las personas internautas de internet no somos únicamente consumidoras indiferentes
las personas internautas de internet no somos únicamente clientas pasivas
las personas internautas de internet no somos únicamente clientas indiferentes
la gente usuarias de internet no somos solo consumidoras pasivas
la gente usuarias de internet no somos solo consumidoras indiferentes
la gente usuarias de internet no somos solo clientas pasivas
la gente usuarias de internet no somos solo clientas indiferentes
la gente usuarias de internet no somos únicamente consumidoras pasivas
la gente usuarias de internet no somos únicamente consumidoras indiferentes
la gente usuarias de internet no somos únicamente clientas pasivas
la gente usuarias de internet no somos únicamente clientas indiferentes
la gente internautas de internet no somos solo consumidoras pasivas
la gente internautas de internet no somos solo consumidoras indiferentes
la gente internautas de internet no somos solo clientas pasivas
la gente internautas de internet no somos solo clientas indiferentes
la gente internautas de internet no somos únicamente consumidoras pasivas
la gente internautas de internet no somos únicamente consumidoras indiferentes
la gente internautas de internet no somos únicamente clientas pasivas
la gente internautas de internet no somos únicamente clientas indiferentes
las mujeres usuarias de internet no somos solo consumidoras pasivas
las mujeres usuarias de internet no somos solo consumidoras indiferentes
las mujeres usuarias de internet no somos solo clientas pasivas
las mujeres usuarias de internet no somos solo clientas indiferentes
las mujeres usuarias de internet no somos únicamente consumidoras pasivas
las mujeres usuarias de internet no somos únicamente consumidoras indiferentes
las mujeres usuarias de internet no somos únicamente clientas pasivas
las mujeres usuarias de internet no somos únicamente clientas indiferentes
las mujeres internautas de internet no somos solo consumidoras pasivas
las mujeres internautas de internet no somos solo consumidoras indiferentes
las mujeres internautas de internet no somos solo clientas pasivas
las mujeres internautas de internet no somos solo clientas indiferentes
las mujeres internautas de internet no somos únicamente consumidoras pasivas
las mujeres internautas de internet no somos únicamente consumidoras indiferentes
las mujeres internautas de internet no somos únicamente clientas pasivas
las mujeres internautas de internet no somos únicamente clientas indiferentes
```

Como se observa, permite un aumento exponencial, pero de no cuidarse
la sintaxis, también da pie a inexactitudes.
