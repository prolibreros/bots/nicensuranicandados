#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'twitter'

$tweet = 1234567890

# Init
client = Twitter::REST::Client.new do |config|
  config.consumer_key        = ''
  config.consumer_secret     = ''
  config.access_token        = ''
  config.access_token_secret = ''
end

# Gets users
users  = []
client.retweeters_of($tweet).each do |user|
  users.push(user.screen_name)
end

# Fetchs users
Dir.chdir(__dir__)
users_new = File.read('src/users_new.txt').split(/\n+/)
users_new = users_new.concat(users)
users_new.sort!
users_new.uniq!

# Saves data
file = File.open('src/users_new.txt', 'w:utf-8')
file.puts users_new
file.close
